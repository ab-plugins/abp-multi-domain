ABP Multi-domain

This MyBB plugin allows admin to set several urls used to access the same forum.

# Installation
* Upload the content of the UPLOAD/ directory to your MyBB root
* Install the plugin from the ACP
* Go to the _ABP Multidomain_ configuration panel
* Add new urls

# TO DO
* Manage admin rights : actually, any admin can manage domains
