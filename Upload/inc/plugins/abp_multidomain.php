<?php

/**
 * ABP Multi-Domain
 * Copyright 2015 CrazyCat
 * */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br />Please make sure IN_MYBB is defined.");
}

define('CN_ABPMD', str_replace('.php', '', basename(__FILE__)));

$plugins->add_hook('admin_config_menu', 'abp_multidomain_admin_config_menu');
$plugins->add_hook('admin_config_action_handler', 'abp_multidomain_admin_config_action_handler');
$plugins->add_hook('admin_config_permissions', 'abp_multidomain_admin_config_permissions');
$plugins->add_hook('admin_load', 'abp_multidomain_admin_load');
$plugins->add_hook('global_start', 'abp_multidomain_global_start');
$plugins->add_hook('pre_output_page', 'abp_multidomain_pre_output_page');

function abp_multidomain_info() {
    global $lang;
    $lang->load(CN_ABPMD);
    return array(
        'name' => $lang->multidomain,
        'description' => $lang->multidomain_desc,
        'website' => 'https://gitlab.com/ab-plugins/abp-multi-domain',
        'author' => 'CrazyCat',
        'authorsite' => 'https://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '0.2',
        'compatibility' => '18*',
        'codename' => CN_ABPMD
    );
}

function abp_multidomain_install() {
    global $db;
    abp_multidomain_uninstall();
    $collation = $db->build_create_table_collation();
    $table = "CREATE TABLE " . TABLE_PREFIX . "multidomain (
		mdid smallint(5) NOT NULL auto_increment,
		bburl varchar(255) NOT NULL,
		homeurl varchar(255) NOT NULL,
		cookiedomain varchar(255) NOT NULL,
		cookiepath VARCHAR(255) NOT NULL,
		cookieprefix VARCHAR(255) NOT NULL,
		mdactive smallint(1) NOT NULL DEFAULT 1,
		PRIMARY KEY (mdid)
		) ENGINE = MYISAM{$collation};
	";
    $db->write_query($table);
    rebuild_settings();
    change_admin_permission('config', 'multidomain', 1);
}

function abp_multidomain_is_installed() {
    global $db;
    return $db->table_exists('multidomain');
}

function abp_multidomain_uninstall() {
    global $db;
    if ($db->table_exists('multidomain')) {
        $db->drop_table('multidomain');
    }
    rebuild_settings();
}

function abp_multidomain_activate() {
// Do nothing
}

function abp_multidomain_deactivate() {
// Do nothing
}

function abp_multidomain_admin_config_menu($sub_menu) {
    global $lang;
    $lang->load(CN_ABPMD);
    $sub_menu[] = array(
        'id' => CN_ABPMD,
        'title' => $lang->multidomain,
        'link' => 'index.php?module=config-multidomain'
    );
    return $sub_menu;
}

function abp_multidomain_admin_config_action_handler(&$actions) {
    $actions['multidomain'] = array(
        'active' => 'multidomain'
    );
}

function abp_multidomain_admin_config_permissions($admin_permissions) {
    global $lang;
    $lang->load(CN_ABPMD);
    $admin_permissions['multidomain'] = $lang->can_manage_multidomain;
    return $admin_permissions;
}

function abp_multidomain_admin_load() {
    global $page, $lang, $db, $mybb;
    $lang->load(CN_ABPMD);

    if ($page->active_action == 'multidomain') {
        $page->add_breadcrumb_item($lang->multidomain, 'index.php?module=config-multidomain');
        if ($mybb->input['action'] == 'do_add' || $mybb->input['action'] == 'do_edit') {
            if (!verify_post_check($mybb->input['my_post_key'])) {
                flash_message($lang->invalid_post_verify_key2, 'error');
                admin_redirect("index.php?module=config-multidomain");
            }
            $action = 'nothing';

            // Add domain
            if ($mybb->input['action'] == 'do_add') {
                $action = 'add';
                $redirect_end = '&amp;action=add';
            }

            // Edit domain
            if ($mybb->input['action'] == 'do_edit') {
                $action = 'edit';
                $mdid = intval($mybb->input['mdid']);
                $query = $db->simple_select('multidomain', '*', "mdid = '{$mdid}'");
                $domain = $db->fetch_array($query);
                $redirect_end = '&amp;action=edit&amp;mdid={$mdid}';
            }

            $bburl = trim($mybb->input['bburl']);
            if (empty($bburl)) {
                flash_message($lang->multidomain_error_bburl, 'error');
                admin_redirect('index.php?module=config-multidomain{$redirect_end}');
            }

            $query = $db->simple_select('multidomain', 'mdid', "LOWER(bburl) = '" . $db->escape_string(strtolower($bburl)) . "'");
            if ($db->num_rows($query) != 0 && !($action == 'edit' && $bburl == $domain['bburl'])) {
                flash_message($lang->multidomain_error_bburl_exists, 'error');
                admin_redirect("index.php?module=config-multidomain{$redirect_end}");
            }

            $insert_update_array = array(
                "bburl" => $db->escape_string($mybb->input['bburl']),
                "homeurl" => $db->escape_string($mybb->input['homeurl']),
                "cookiedomain" => $db->escape_string($mybb->input['cookiedomain']),
                "cookiepath" => $db->escape_string($mybb->input['cookiepath']),
                "cookieprefix" => $db->escape_string($mybb->input['cookieprefix']),
                'mdactive' => (int) $mybb->input['mdactive']
            );

            if ($action == 'edit') {
                $db->update_query('multidomain', $insert_update_array, "mdid = '{$mdid}'");
                $redirect = $lang->multidomain_success_edit;
            } elseif ($action == 'add') {
                $db->insert_query("multidomain", $insert_update_array);
                $redirect = $lang->multidomain_success_add;
            }
            flash_message($redirect, 'success');
            admin_redirect("index.php?module=config-multidomain");
        } elseif ($mybb->input['action'] == 'add' || $mybb->input['action'] == 'edit') {
            $action = $mybb->input['action'];

            $page->output_header($lang->multidomain);
            $sub_tabs = array();

            $sub_tabs['multidomain'] = array(
                'title' => $lang->multidomain,
                'link' => "index.php?module=config-multidomain",
                'description' => $lang->multidomain_nav
            );

            if ($action == 'add') {
                $domain = array(
                    'bburl' => '',
                    'homeurl' => '',
                    'cookiedomain' => '',
                    'cookiepath' => '/',
                    'cookieprefix' => '',
                    'mdactive' => 0
                );
                $page->add_breadcrumb_item($lang->multidomain_add, "index.php?module=config-multidomain&action=add");
                $sub_tabs['multidomain_add'] = array(
                    'title' => $lang->multidomain_add,
                    'link' => "index.php?module=config-multidomain&amp;action=add",
                    'description' => $lang->multidomain_add_nav
                );
                $page->output_nav_tabs($sub_tabs, "multidomain_add");
                $form = new Form("index.php?module=config-multidomain&amp;action=do_add", "post", "", 1);
                $form_container = new FormContainer($lang->multidomain_add);
            }
            if ($action == 'edit') {
                $mdid = intval($mybb->input['mdid']);
                $page->add_breadcrumb_item($lang->multidomain_edit, "index.php?module=config-multidomain&action=edit&mdid={$mdid}");
                $query = $db->simple_select("multidomain", "*", "mdid = {$mdid}");
                $domain = $db->fetch_array($query);
                $sub_tabs['multidomain_edit'] = array(
                    'title' => $lang->multidomain_edit,
                    'link' => "index.php?module=config-multidomain&amp;action=edit&amp;mdid={$mdid}",
                    'description' => $lang->multidomain_edit_nav
                );
                $page->output_nav_tabs($sub_tabs, "multidomain_edit");
                $form = new Form("index.php?module=config-multidomain&amp;action=do_edit", "post", "", 1);
                $form_container = new FormContainer($lang->multidomain_edit);
            }

            $table = new Table;
            $form_container->output_row(
                    $lang->multidomain_bburl . " <em>*</em>", $lang->multidomain_bburl_desc, $form->generate_text_box("bburl", $domain['bburl'])
            );
            $form_container->output_row(
                    $lang->multidomain_homeurl . " <em>*</em>", $lang->multidomain_homeurl_desc, $form->generate_text_box("homeurl", $domain['homeurl'])
            );
            $form_container->output_row(
                    $lang->multidomain_cookiedomain . " <em>*</em>", $lang->multidomain_cookiedomain_desc, $form->generate_text_box("cookiedomain", $domain['cookiedomain'])
            );
            $form_container->output_row(
                    $lang->multidomain_cookiepath . " <em>*</em>", $lang->multidomain_cookiepath_desc, $form->generate_text_box('cookiepath', $domain['cookiepath'])
            );
            $form_container->output_row(
                    $lang->multidomain_cookieprefix . " <em>*</em>", $lang->multidomain_cookieprefix_desc, $form->generate_text_box('cookieprefix', $domain['cookieprefix'])
            );
            $form_container->output_row(
                    $lang->multidomain_mdactive, $lang->multidomain_mdactive_desc, $form->generate_check_box('mdactive', 1, '', array('checked' => $domain['mdactive']))
            );

            $form_container->end();

            if ($action == 'edit') {
                echo $form->generate_hidden_field("mdid", $mdid);
            }

            $buttons[] = $form->generate_submit_button($lang->multidomain_submit);
            $form->output_submit_wrapper($buttons);
            $form->end();
            $page->output_footer();
        } elseif ($mybb->input['action'] == "do_delete") {
            if ($mybb->input['no']) {
                admin_redirect("index.php?module=config-multidomain");
            } else {
                if (!verify_post_check($mybb->input['my_post_key'])) {
                    flash_message($lang->invalid_post_verify_key2, 'error');
                    admin_redirect("index.php?module=config-multidomain");
                }

                $mdid = intval($mybb->input['mdid']);
                $query = $db->simple_select('multidomain', '*', "mdid = '{$mdid}'");
                $domain = $db->fetch_array($query);

                $db->delete_query('multidomain', "mdid = '{$mdid}'");
                multidomain_redoconf();

                flash_message($lang->multidomain_success_delete, 'success');
                admin_redirect('index.php?module=config-multidomain');
            }
        } elseif ($mybb->input['action'] == 'delete') {
            $page->output_confirm_action("index.php?module=config-multidomain&action=do_delete&mdid={$mybb->input['mdid']}&my_post_key={$mybb->post_code}", $lang->multidomain_delete);
        } else {
            $page->output_header($lang->multidomain);

            $sub_tabs = array();
            $sub_tabs['multidomain'] = array(
                'title' => $lang->multidomain,
                'link' => 'index.php?module=config-multidomain',
                'description' => $lang->multidomain_nav
            );
            $sub_tabs['multidomain_add'] = array(
                'title' => $lang->multidomain_add,
                'link' => 'index.php?module=config-multidomain&amp;action=add',
                'description' => $lang->multidomain_add_nav
            );
            $page->output_nav_tabs($sub_tabs, 'multidomain');
            $query = $db->simple_select('multidomain', '*', "", array('order_by' => 'bburl', 'order_dir' => 'ASC'));

            if ($db->num_rows($query) > 0) {
                $table = new Table;

                $table->construct_header($lang->multidomain_bburl, array('class' => 'align_center'));
                $table->construct_header($lang->multidomain_homeurl, array('class' => 'align_center'));
                $table->construct_header($lang->multidomain_mdactive, array('class' => 'align_center'));
                $table->construct_header($lang->controls, array("width" => "30%", "colspan" => 2, 'class' => 'align_center'));

                while ($domain = $db->fetch_array($query)) {
                    $table->construct_cell($domain['bburl']);
                    $table->construct_cell($domain['homeurl']);
                    $table->construct_cell('<img src="../images/abpmd_' . (($domain['mdactive'] == 0) ? 'off' : 'on') . '.png" alt="" title="' . (($domain['mdactive'] == 0) ? $lang->no : $lang->yes) . '" />', array('class' => 'align_center'));
                    $table->construct_cell("<a href=\"index.php?module=config-multidomain&amp;action=edit&amp;mdid={$domain['mdid']}&amp;my_post_key={$mybb->post_code}\">{$lang->edit}</a>", array('class' => 'align_center', 'width' => '15%'));
                    $table->construct_cell("<a href=\"index.php?module=config-multidomain&amp;action=delete&amp;mdid={$domain['mdid']}\">{$lang->delete}</a>", array('class' => 'align_center', 'width' => '15%'));
                    $table->construct_row();
                }
                $table->output($lang->multidomain);
            }
            $page->output_footer();
        }
    }
}

function abp_multidomain_global_start() {
    global $mybb, $db;
    $currenturl = $db->escape_string(strtolower($_SERVER['HTTP_HOST']));
    $query = $db->simple_select('multidomain', '*', "bburl LIKE 'http%://" . $currenturl . "%' AND mdactive=1");
    if ($db->num_rows($query) >= 1) {
        $newsettings = $db->fetch_array($query);
        $mybb->settings['oldbburl'] = strtolower($mybb->settings['bburl']);
        $mybb->settings['oldhomeurl'] = strtolower($mybb->settings['homeurl']);
        $mybb->settings['bburl'] = strtolower($newsettings['bburl']);
        $mybb->settings['homeurl'] = strtolower($newsettings['homeurl']);
        $mybb->settings['cookiedomain'] = $newsettings['cookiedomain'];
        $mybb->settings['cookiepath'] = $newsettings['cookiepath'];
        $mybb->settings['cookieprefix'] = $newsettings['cookieprefix'];
        $mybb->asset_url = str_replace($mybb->settings['oldbburl'], $mybb->settings['bburl'], $mybb->asset_url);
    }
}

function abp_multidomain_pre_output_page(&$contents) {
    global $mybb;
    if (!empty($mybb->settings['oldbburl'])) {
        $contents = str_replace(
            [$mybb->settings['oldhomeurl'], $mybb->settings['oldbburl']],
            [$mybb->settings['homeurl'], $mybb->settings['bburl']],
            $contents
        );
    }
}
